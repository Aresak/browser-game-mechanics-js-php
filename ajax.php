<?php
/**
 * Created by PhpStorm.
 * User: Aresak
 * Date: 7/12/2015
 * Time: 12:00 PM
 */



    if($_POST) {
        define("allowed_to_view_database_info", true);
        define("logged_in", true);
        include "database.php";
        include "func.php";

        if(!isset($_POST["request"])) {
            die();
        }

        $request = $_POST["request"];
        $player_id = 1; // Because we do not have multiple accounts, we got only 1

        if($request == "increase_skill") {
            /*
             * Stats:
             * Movements speed
             * Damage
             * Health
             * Stamina
             * Weight
             *
             * Base price: 1
             *
             */

            // Get list of current stats
            $connection = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
                or die(mysqli_error($connection));

            $query = "SELECT * FROM lotfi_stats WHERE player_id='$player_id'";
            $result = mysqli_query($connection, $query)
                or die(mysqli_error($connection));

            $force          = mysqli_result($result, 0, "force");
            $agility        = mysqli_result($result, 0, "agility");
            $endurance      = mysqli_result($result, 0, "endurance");
            $speed          = mysqli_result($result, 0, "speed");
            $charge         = mysqli_result($result, 0, "charge");
            $gold           = mysqli_result($result, 0, "gold");
            $experience     = mysqli_result($result, 0, "experience");


            // Has gold now?
            if(!isset($_POST["skill"])) {
                mysqli_close($connection);
                die("Error: No skill selected!");
            }

            $skill = $_POST["skill"];

            if($skill == "force") {
                if($gold >= getPrice(10, ($force))) {
                    // Tadaa, you can increase this skill

                    $spend = $gold - getPrice(10, ($force));

                    $force ++;
                    $query = "UPDATE lotfi_stats
                    SET `force`='$force', gold='$spend' WHERE player_id='$player_id'";

                    $result = mysqli_query($connection, $query)
                        or die(mysqli_error($connection));

                    addRandomExperience($connection, $force, $player_id);

                    mysqli_close($connection);
                    die("success");
                }

                else {
                    mysqli_close($connection);
                    $spend = $gold - getPrice(10, ($force));
                    die("Error: No gold ($spend)");
                }
            }

            else if($skill == "agility") {
                if($gold >= getPrice(10, ($agility))) {

                    $spend = $gold - getPrice(10, ($agility));

                    $agility ++;
                    $query = "UPDATE lotfi_stats
                    SET agility='$agility', gold='$spend' WHERE player_id='$player_id'";

                    $result = mysqli_query($connection, $query)
                        or die(mysqli_error($connection));

                    addRandomExperience($connection, $agility, $player_id);

                    mysqli_close($connection);
                    die("success");
                }
                else {
                    mysqli_close($connection);
                    $spend = $gold - getPrice(10, ($agility));
                    die("Error: No gold ($spend)");
                }
            }

            else if($skill == "endurance") {
                if($gold >= getPrice(10, ($endurance))) {

                    $spend = $gold - getPrice(10, ($endurance));

                    $endurance ++;
                    $query = "UPDATE lotfi_stats
                    SET endurance='$endurance', gold='$spend' WHERE player_id='$player_id'";

                    $result = mysqli_query($connection, $query)
                        or die(mysqli_error($connection));

                    addRandomExperience($connection, $endurance, $player_id);

                    mysqli_close($connection);
                    die("success");
                }
                else {
                    mysqli_close($connection);
                    $spend = $gold - getPrice(10, ($endurance));
                    die("Error: No gold ($spend)");
                }
            }

            else if($skill == "speed") {
                if($gold >= getPrice(10, ($speed))) {

                    $spend = $gold - getPrice(10, ($speed));
                    $speed ++;

                    $query = "UPDATE lotfi_stats
                    SET speed='$speed', gold='$spend' WHERE player_id='$player_id'";

                    $result = mysqli_query($connection, $query)
                    or die(mysqli_error($connection));

                    addRandomExperience($connection, $speed, $player_id);

                    mysqli_close($connection);
                    die("success");
                }
                else {
                    mysqli_close($connection);
                    $spend = $gold - getPrice(10, ($speed));
                    die("Error: No gold ($spend)");
                }
            }

            else if($skill == "charge") {
                if($gold >= getPrice(10, ($charge))) {

                    $spend = $gold - getPrice(10, ($charge));
                    $charge ++;

                    $query = "UPDATE lotfi_stats
                    SET charge='$charge', gold='$spend' WHERE player_id='$player_id'";

                    $result = mysqli_query($connection, $query)
                    or die(mysqli_error($connection));

                    addRandomExperience($connection, $charge, $player_id);

                    mysqli_close($connection);
                    die("success");
                }
                else {
                    mysqli_close($connection);
                    $spend = $gold - getPrice(10, ($charge));
                    die("Error: No gold ($spend)");
                }
            }

            else {
                mysqli_close($connection);
                die("Error: Skill not found");
            }
        }

        else if($request == "buy_item") {
            if(!isset($_POST["id"])) {
                mysqli_close($connection);
                die("Error: No item ID found");
            }


            $connection = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
                or die(mysqli_error($connection));
        }

        else if($request == "sell_item") {
            $connection = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
                or die(mysqli_error($connection));



        }

        else if($request == "refresh_shop") {
            $connection = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
                or die(mysqli_error($connection));

            // Get list of possible items

            $query = "SELECT * FROM lotfi_items";
            $result = mysqli_query($connection, $query)
                or die(mysqli_error($connection));

            $items;

            for($i = 0; $i < mysqli_num_rows($result); $i ++) {
                $items[$i]["name"]      = mysqli_result($result, $i, "name");
                $items[$i]["id"]        = mysqli_result($result, $i, "ID");
            }

            if(logged_in) {
                // We will check if the user has any golds,
                // if so, we will decrease it's amount by preset
                // refresh price. If not, give an error

                // Get list of current stats so we can get amount of golds
                $query = "SELECT * FROM lotfi_stats WHERE player_id='$player_id'";
                $result = mysqli_query($connection, $query)
                or die(mysqli_error($connection));

                /*$movement_speed = mysqli_result($result, 0, "movement_speed");
                $damage         = mysqli_result($result, 0, "damage");
                $weight         = mysqli_result($result, 0, "weight");
                $health         = mysqli_result($result, 0, "health");
                $stamina        = mysqli_result($result, 0, "stamina");*/
                $gold           = mysqli_result($result, 0, "gold");

                // Check it out now
                if($gold >= $refresh_price) {

                    $query = "UPDATE lotfi_stats SET gold="
                        . ($gold - $refresh_price) .
                        " WHERE player_id='$player_id'";
                    $result = mysqli_query($connection, $query)
                    or die(mysqli_error($connection));

                    // Refresh it now
                    $newData = generateNewItems($items, 5);

                    $newData = json_encode($newData);

                    $query = "UPDATE lotfi_shop SET `data`='$newData', last_refresh='" . time() . "' WHERE player_id='$player_id'";
                    $result = mysqli_query($connection, $query)
                        or die(mysqli_error($connection));

                    mysqli_close($connection);
                    die("success");
                }
                else {
                    mysqli_close($connection);
                    die("Error: No gold");
                }

            }
            else {
                // Is probably server so we will refresh it without any golds

                // Refresh it now
                $newData = generateNewItems($items, 5);
                $newData = json_encode($newData);

                $query = "UPDATE lotfi_shop SET `data`='$newData', last_refresh='" . time() . "' WHERE player_id='$player_id'";
                $result = mysqli_query($connection, $query)
                or die(mysqli_error($connection));

                mysqli_close($connection);
            }
        }

        else if($request == "debug") {
            $connection = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
                or die(mysqli_error($connection));

            $request2 = $_POST["request2"];

            if($request2 == "add_golds") {
                $query = "SELECT * FROM lotfi_stats WHERE player_id='$player_id'";
                $result = mysqli_query($connection, $query)
                or die(mysqli_error($connection));

                /*$movement_speed = mysqli_result($result, 0, "movement_speed");
                $damage         = mysqli_result($result, 0, "damage");
                $weight         = mysqli_result($result, 0, "weight");
                $health         = mysqli_result($result, 0, "health");
                $stamina        = mysqli_result($result, 0, "stamina");*/
                $gold           = mysqli_result($result, 0, "gold");

                $query = "UPDATE lotfi_stats SET gold="
                    . ($_POST["add"] + $gold) .
                    " WHERE player_id='$player_id'";
                $result = mysqli_query($connection, $query)
                    or die(mysqli_error($connection));
            }
        }

        else die();
    }
    else die();