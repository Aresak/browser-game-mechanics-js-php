<?php
/**
 * Created by PhpStorm.
 * User: Aresak
 * Date: 7/12/2015
 * Time: 11:59 AM
 */

// fetchin data
define("allowed_to_view_database_info", true);
define("logged_in", true);
include "database.php";
include "func.php";

// Debug player id
$player_id = 1;

$connection = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
or die(mysqli_error($connection));

$query = "SELECT * FROM lotfi_stats WHERE player_id='$player_id'";
$result = mysqli_query($connection, $query)
or die(mysqli_error($connection));

$force          = mysqli_result($result, 0, "force");
$agility        = mysqli_result($result, 0, "agility");
$endurance      = mysqli_result($result, 0, "endurance");
$speed          = mysqli_result($result, 0, "speed");
$charge         = mysqli_result($result, 0, "charge");
$gold           = mysqli_result($result, 0, "gold");
$experience     = mysqli_result($result, 0, "experience");



$force_bonus    = 0;
$agility_bonus  = 0;
$endurance_bonus= 0;
$speed_bonus    = 0;
$charge_bonus   = 0;

$force_total    = $force + $force_bonus;
$agility_total  = $agility + $agility_bonus;
$endurance_total= $endurance + $endurance_bonus;
$speed_total    = $speed + $speed_bonus;
$charge_total   = $charge + $charge_bonus;

$force_price    = getPrice(getBaseSkill(), $force);
$agility_price  = getPrice(getBaseSkill(), $agility);
$endurance_price= getPrice(getBaseSkill(), $endurance);
$speed_price    = getPrice(getBaseSkill(), $speed);
$charge_price   = getPrice(getBaseSkill(), $charge);

$stats          = getStats($force_total, $agility_total, $endurance_total, $speed_total, $charge_total, $gold);
$life           = getStatLife($stats);
$criticalDamage  = getStatCrital($stats);
$damagePerSec   = getStatDamagePerSecond($stats);

?>

<html>
<head>
    <title>Lotfi Arena</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="lotfi.css">
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script>
        var lotfi = {
            basePriceSkill: 0<?php echo getBaseSkill(); ?>,
            force: 0<?php echo $force;?>,
            agility: 0<?php echo $agility;?>,
            endurance: 0<?php echo $endurance;?>,
            speed: 0<?php echo $speed;?>,
            charge: 0<?php echo $charge;?>,
            experience: 0<?php echo $experience;?>,
            gold: 0<?php echo $gold;?>,

            train: function(name) {
                $.post("ajax.php", {request: "increase_skill", skill: name}, function(data) {
                    if(data == "" || data == "success") {
                        // Well done, let's update our stats
                        lotfi.update_stats(name)
                    }
                    else {
                        alert(data);
                        console.error(data);
                    }
                }, "html");
            },

            update_stats: function(name) {
                // This function is called after increasing a stat
                if(name == "force")         {
                    alert("Old: " + lotfi.gold + " Now: " + (lotfi.gold - getPrice(lotfi.basePriceSkill, lotfi.force)));
                    $("#player_gold").text(lotfi.gold - getPrice(lotfi.basePriceSkill, lotfi.force));
                    lotfi.force ++;
                }
                else if(name == "agility")       {
                    $("#player_gold").text(lotfi.gold - getPrice(lotfi.basePriceSkill, lotfi.agility));
                    lotfi.agility ++;
                }
                else if(name == "endurance")     {
                    $("#player_gold").text(lotfi.gold - getPrice(lotfi.basePriceSkill, lotfi.endurance));
                    lotfi.endurance ++;
                }
                else if(name == "speed")         {
                    $("#player_gold").text(lotfi.gold - getPrice(lotfi.basePriceSkill, lotfi.speed));
                    lotfi.speed ++;
                }
                else if(name == "charge")        {
                    $("#player_gold").text(lotfi.gold - getPrice(lotfi.basePriceSkill, lotfi.charge));
                    lotfi.charge ++;
                }


                $("#force-static-value").text(lotfi.force);
                $("#agility-static-value").text(lotfi.agility);
                $("#endurance-static-value").text(lotfi.endurance);
                $("#speed-static-value").text(lotfi.speed);
                $("#charge-static-value").text(lotfi.charge);
                $("#experience-static-value").text(lotfi.experience);

                $("#force-total-value").text(lotfi.force + $("#force-bonus-value").text());
                $("#agility-total-value").text(lotfi.agility + $("#agility-bonus-value").text());
                $("#endurance-total-value").text(lotfi.endurance + $("#endurance-bonus-value").text());
                $("#speed-total-value").text(lotfi.speed + $("#speed-bonus-value").text());
                $("#charge-total-value").text(lotfi.charge + $("#charge-bonus-value").text());
                $("#experience-total-value").text(lotfi.experience);
                $("#critdmg-total-value").text(getCriticalDamage());
                $("#dmps-total-value").text(getDamagePerSecond());
                $("#life-total-value").text(getLife());

                $("#force-train-price").text(getPrice(lotfi.basePriceSkill, lotfi.force));
                $("#agility-train-price").text(getPrice(lotfi.basePriceSkill, lotfi.agility));
                $("#endurance-train-price").text(getPrice(lotfi.basePriceSkill, lotfi.endurance));
                $("#speed-train-price").text(getPrice(lotfi.basePriceSkill, lotfi.speed));
                $("#charge-train-price").text(getPrice(lotfi.basePriceSkill, lotfi.charge));

            }
        };

        function train(name) {
            lotfi.train(name);
        }
    </script>
    <script src="lotfi.js"></script>
</head>

<body>
<button id="back-button" onClick="location.href = '/';">
    Back to index
</button>


<div id="menu">
    <?php
    // Php based menu
    // Debug form
    echo "<form method='POST' action='make_it_easy.php'>";
    echo "<input type='text' name='golds' placeholder='add golds'>";
    echo "<input type='hidden' name='callback' value='arena.php'>";
    echo "</form>";

    // Debug time
    echo time();

    echo " | ";

    // golds
    echo "Gold: <span id='player_gold'>" . $gold . "</span>";

    echo " | ";

    // Name last
    echo "PlayerName";
    ?>
</div>

<br><br><br>

<div id="train">
    <div class="train-item">
        Force: (

        <span id="force-static-value" class="stat-static">
            <?php
                echo $force;
            ?>
        </span>|<span id="force-bonus-value" class="stat-dynamic">
            <?php
                echo $force_bonus;
            ?>
        </span>) <span id="force-total-value" class="stat-total">
            <?php
                echo $force_total;
            ?>
        </span>
        ---- <button id="force-train-button" onClick="train('force');">Train! $<span id="force-train-price"><?php echo $force_price; ?></span></button>
    </div>

    <div class="train-item">
        Agility: (

        <span id="agility-static-value" class="stat-static">
            <?php
                echo $agility;
            ?>
        </span>|<span id="agility-bonus-value" class="stat-dynamic">
            <?php
                echo $agility_bonus;
            ?>
        </span>) <span id="agility-total-value" class="stat-total">
            <?php
                echo $agility_total;
            ?>
        </span>
        ---- <button id="agility-train-button" onClick="train('agility');">Train! $<span id="agility-train-price"><?php echo $agility_price; ?></span></button>
    </div>

    <div class="train-item">
        Endurance: (

        <span id="endurance-static-value" class="stat-static">
            <?php
                echo $endurance;
            ?>
        </span>|<span id="endurance-bonus-value" class="stat-dynamic">
            <?php
                echo $endurance_bonus;
            ?>
        </span>) <span id="endurance-total-value" class="stat-total">
            <?php
                echo $endurance_total;
            ?>
        </span>
        ---- <button id="endurance-train-button" onClick="train('endurance');">Train! $<span id="endurance-train-price"><?php echo $endurance_price; ?></span></button>
    </div>

    <div class="train-item">
        Speed: (

        <span id="speed-static-value" class="stat-static">
            <?php
                echo $speed;
            ?>
        </span>|<span id="speed-bonus-value" class="stat-dynamic">
            <?php
                echo $speed_bonus;
            ?>
        </span>) <span id="speed-total-value" class="stat-total">
            <?php
                echo $speed_total;
            ?>
        </span>
        ---- <button id="speed-train-button" onClick="train('speed');">Train! $<span id="speed-train-price"><?php echo $speed_price; ?></span></button>
    </div>

    <div class="train-item">
        Charge: (

        <span id="charge-static-value" class="stat-static">
            <?php
                echo $charge;
            ?>
        </span>|<span id="charge-bonus-value" class="stat-dynamic">
            <?php
                echo $charge_bonus;
            ?>
        </span>) <span id="charge-total-value" class="stat-total">
            <?php
                echo $charge_total;
            ?>
        </span>
        ---- <button id="charge-train-button" onClick="train('charge');">Train! $<span id="charge-train-price"><?php echo $charge_price; ?></span></button>
    </div>

    <div class="train-item">
        Life: <span id="life-total-value" class="stat-total">
            <?php
                echo $life;
            ?>
        </span>
    </div>

    <div class="train-item">
        Damage/Second: <span id="dmps-total-value" class="stat-total">
            <?php
                echo $damagePerSec;
            ?>
        </span>
    </div>

    <div class="train-item">
        Critical Damage: <span id="critdmg-total-value" class="stat-total">
            <?php
                echo $criticalDamage;
            ?>
        </span>
    </div>

    <div class="train-item">
        Experiences: <span id="experience-total-value" class="stat-total">
            <?php
                echo $experience;
            ?>
        </span>
    </div>
</div>
</body>
</html>