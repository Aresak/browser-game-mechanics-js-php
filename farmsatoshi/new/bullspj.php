<?php
/**
 * Created by PhpStorm.
 * User: Aresak
 * Date: 7/19/2015
 * Time: 4:58 PM
 */

class BullDebug {
    private static $lastErrorMessage, $lastMessage;

    public static function error() {
        return BullDebug::$lastErrorMessage;
    }

    public static function message() {
        return BullDebug::$lastMessage;
    }

    public static function addError($mess) {
        BullDebug::$lastErrorMessage    = $mess;
    }

    public static function addMsg($mess) {
        BullDebug::$lastMessage         = $mess;
    }
}

class BullSQL {
    public static $user = "";
    public static $host = "";
    public static $pass = "";
    public static $base = "";

    private $con;

    public function __construct() {
        $this->con = mysqli_connect(BullSQL::$host, BullSQL::$user, BullSQL::$pass, BullSQL::$base)
        or die(mysqli_error($this->con));
    }

    public function connection() {
        return $this->con;
    }

    public static function returnNewConnection() {
        return new BullSQL();
    }

    public static function mysqli_result($result,$row,$field=0) {
        if ($result===false) return false;
        if ($row>=mysqli_num_rows($result)) return false;
        if (is_string($field) && !(strpos($field,".")===false)) {
            $t_field=explode(".",$field);
            $field=-1;
            $t_fields=mysqli_fetch_fields($result);
            for ($id=0;$id<mysqli_num_fields($result);$id++) {
                if ($t_fields[$id]->table==$t_field[0] && $t_fields[$id]->name==$t_field[1]) {
                    $field=$id;
                    break;
                }
            }
            if ($field==-1) return false;
        }
        mysqli_data_seek($result,$row);
        $line=mysqli_fetch_array($result);
        return isset($line[$field])?$line[$field]:false;
    }
}

class BullUtil {
    public static function getPrice($base, $level) {
        $res = $base;
        for($i = 0; $i < $level; $i++) {
            $res += $base + $res;
        }

        return $res;
    }

    public static function getItemId($name, $connection = "") {
        if($connection == "") {
            if(!isset(BullSQL::$user)) return;
            if(!isset(BullSQL::$user)) return;
            else {
                $connection = mysqli_connect(BullSQL::$host, BullSQL::$user, BullSQL::$pass, BullSQL::$base)
                or die(mysqli_error($connection));
            }
        }

        $query = "SELECT * FROM lotfi_items";
        $result = mysqli_query($connection, $query)
        or die(mysqli_error($connection));


        for($a = 0; $a < mysqli_num_rows($result); $a ++) {
            if(BullSQL::mysqli_result($result, $a, "name") == $name) {
                return BullSQL::mysqli_result($result, $a, "ID");
            }
        }
    }

    public static function getItemName($id, $connection = "") {
        if($connection == "") {
            if(!isset(BullSQL::$user)) return;
            else {
                $connection = mysqli_connect(BullSQL::$host, BullSQL::$user, BullSQL::$pass, BullSQL::$base)
                or die(mysqli_error($connection));
            }
        }

        $query = "SELECT * FROM lotfi_items";
        $result = mysqli_query($connection, $query)
        or die(mysqli_error($connection));


        for($a = 0; $a < mysqli_num_rows($result); $a ++) {
            if(BullSQL::mysqli_result($result, $a, "ID") == $id) {
                return BullSQL::mysqli_result($result, $a, "name");
            }
        }
    }

    public static function generateNewItems($items, $ax, $ay) {
        $result = array();

        for($x = 0; $x < $ax; $x ++) {
            for($y = 0; $y < $ay; $y ++) {
                $randomness = rand(0, (sizeof($items) - 1));
                $result[$x][$y]["id"]       = $items[$randomness]["id"];
                $result[$x][$y]["name"]     = $items[$randomness]["name"];
                $result[$x][$y]["level"]    = rand(0, 25);
                $result[$x][$y]["gems"]     = BullUtil::generateRandomGems(BullUtil::getListOfGems(), 50, 4, $result[$x][$y]["level"]);
                $result[$x][$y]["price"]    = $result[$x][$y]["level"] * 4.77 * (sizeof($result[$x][$y]["gems"]) + 1);
            }
        }

        return $result;
    }

    public static function generateRandomGems($gems, $chance, $max, $level) {
        $res = array();

        $a = 0;
        for($i = 0; $i < $max; $i ++) {
            $rand = rand(0, 100);
            $a ++;
            if(($chance / $a) >= $rand) {
                $a --;
                $gemID = rand(0, (sizeof($gems) - 1));

                $res[$a]["name"]    = $gems[$gemID]["name"];
                $res[$a]["bonus"]   = rand($gems[$gemID]["minBonus"], $gems[$gemID]["maxBonus"] * $level);
                $res[$a]["id"]      = $gems[$gemID]["id"];
                $res[$a]["stat"]    = $gems[$gemID]["stat"];
            }
            // Else no gem added
        }

        return $res;
    }

    public static function addItemToInventory($inventory, $item) {
        $result = array();

        for($i = 0; $i < sizeof($inventory); $i ++) {
            $result[$i]["name"]         = $inventory[$i]["name"];
            $result[$i]["id"]           = $inventory[$i]["id"];
            $result[$i]["level"]        = $inventory[$i]["level"];
            $result[$i]["price"]        = $inventory[$i]["price"];
            $result[$i]["gems"]         = $inventory[$i]["gems"];
        }

        $result[(sizeof($inventory) + 1)]["name"]   = $item["name"];
        $result[(sizeof($inventory) + 1)]["id"]     = $item["id"];
        $result[(sizeof($inventory) + 1)]["level"]  = $item["level"];
        $result[(sizeof($inventory) + 1)]["price"]  = $item["price"];
        $result[(sizeof($inventory) + 1)]["gems"]   = $item["gems"];

        return $result;
    }

    public static function getStatLife($stats) {
        return ($stats["force"] * 2.1 + $stats["endurance"] * 3) / 1.5 + $stats["experience"] * 0.01;
    }

    public static function getStatDamagePerSecond($stats) {
        return ($stats["speed"] * 0.4 + $stats["force"] * 1.4) * 0.77 + 0.02;
    }

    public static function getStatCritical($stats) {
        return pow($stats["force"] * $stats["speed"], 2) / 1000 + $stats["agility"] * 6 + $stats["experience"] * 0.01;
    }

    public static function getStats($force, $agility, $endurance, $speed, $charge, $experience, $gold = "") {
        $result["force"] = $force;
        $result["agility"] = $agility;
        $result["endurance"] = $endurance;
        $result["speed"] = $speed;
        $result["charge"] = $charge;
        $result["experience"] = $experience;
        $result["gold"] = $gold;

        return $result;
    }

    public static function emptyItem() {
        $res = array();

        $res["id"]      = 0;
        $res["name"]    = "Empty";
        $res["level"]   = 0;
        $res["price"]   = 0;
        $res["gems"]    = array();

        return $res;
    }

    public static function emptyInventory($size_x, $size_y) {
        $res = array();
        for($x = 0; $x < $size_x; $x ++) {
            for($y = 0; $y < $size_y; $y ++) {
                $res[$x][$y] = BullUtil::emptyItem();
            }
        }

        return $res;
    }

    public static function getListOfGems($con = "") {
        if(empty($con)) {
            // Generate a new connection
            $con = new BullSQL();
            $custom = true;
        }
        else $custom = false;

        $query  = "SELECT * FROM lotfi_gems";
        $result = mysqli_query($con->connection(), $query) or die(mysqli_error($con->connection()));

        $res = array();

        for($i = 0; $i < mysqli_num_rows($result); $i ++) {
            $res[$i]["id"]          = BullSQL::mysqli_result($result, $i, "ID");
            $res[$i]["name"]        = BullSQL::mysqli_result($result, $i, "name");
            $res[$i]["minBonus"]    = BullSQL::mysqli_result($result, $i, "minBonus");
            $res[$i]["maxBonus"]    = BullSQL::mysqli_result($result, $i, "maxBonus");
            $res[$i]["stat"]        = BullSQL::mysqli_result($result, $i, "stat");
        }

        if($custom) mysqli_close($con->connection());
        return $res;
    }

    public static function getListOfItems($con = "") {
        if(empty($con)) {
            // Generate a new connection
            $con = new BullSQL();
            $custom = true;
        }
        else $custom = false;

        $query  = "SELECT * FROM lotfi_items";
        $result = mysqli_query($con->connection(), $query) or die(mysqli_error($con->connection()));

        $res = array();

        for($i = 0; $i < mysqli_num_rows($result); $i ++) {
            $res[$i]["id"]          = BullSQL::mysqli_result($result, $i, "ID");
            $res[$i]["name"]        = BullSQL::mysqli_result($result, $i, "name");
        }

        if($custom) mysqli_close($con->connection());
        return $res;
    }
}

class Bull {
    public $force, $agility, $endurance, $speed, $charge, $experience, $gold, $player_id;

    public function __construct($player_id) {
        $this->player_id = $player_id;
        $this->updateStats();
    }

    public function updateStats() {
        // Code to update stats from the database
        // Use BullSQL

        $con = new BullSQL();
        $query = "SELECT * FROM lotfi_stats WHERE player_id='" . $this->player_id . "'";
        $result = mysqli_query($con->connection(), $query) or die(mysqli_error($con->connection()));

        // Save the new data
        $this->force = BullSQL::mysqli_result($result, 0, "force");
        $this->agility = BullSQL::mysqli_result($result, 0, "agility");
        $this->endurance = BullSQL::mysqli_result($result, 0, "endurance");
        $this->speed = BullSQL::mysqli_result($result, 0, "speed");
        $this->charge = BullSQL::mysqli_result($result, 0, "charge");
        $this->experience = BullSQL::mysqli_result($result, 0, "experience");
        $this->gold = BullSQL::mysqli_result($result, 0, "gold");

        mysqli_close($con->connection());
    }

    public function addGold($gold) {
        $con = new BullSQL();

        $query = "UPDATE lotfi_stats SET gold = gold + " . $gold . " WHERE player_id = '$this->player_id'";
        $result = mysqli_query($con->connection(), $query) or die(mysqli_error($con->connection()));

        mysqli_close($con->connection());
    }

    public function returnStats() {
        return BullUtil::getStats($this->force, $this->agility, $this->endurance, $this->speed, $this->charge, $this->experience, $this->gold);
    }

    public static function addRandomExperience($connection, $base, $where_id = "") {
        $min    = 1 * $base;
        $max    = $base * $base;

        $total  = rand($min, $max);

        $query  = "UPDATE lotfi_stats SET experience = experience + $total WHERE player_id='$where_id'";
        $result = mysqli_query($connection, $query)
                or die(
                    mysqli_error($connection)
                );

        return $total;
    }

    public static function createNewBull() {
        $bullInventory  = BullUtil::emptyInventory(5, 4);
        $con            = new BullSQL();
        $items          = BullUtil::getListOfItems($con);
        $bullStock      = BullUtil::generateNewItems($items, 5, 4);


        // Create tables now
        // Stats
        $rememberID     = rand(111111111, 999999999);
        $queryStats     = "INSERT INTO lotfi_stats (`force`, rememberid) VALUES (1, $rememberID)";
        $resultStats    = mysqli_query($con->connection(), $queryStats) or die(mysqli_error($con->connection()));

        $queryID        = "SELECT * FROM lotfi_stats WHERE rememberid='$rememberID'";
        $resultID       = mysqli_query($con->connection(), $queryID) or die(mysqli_error($con->connection()));

        $player_id      = BullSQL::mysqli_result($resultID, 0, "ID");


        $queryRD        = "UPDATE lotfi_stats SET player_id = '$player_id' WHERE ID = '$player_id'";
        $resultRD       = mysqli_query($con->connection(), $queryRD) or die(mysqli_error($con->connection()));


        // Shop
        $queryShop      = "INSERT INTO lotfi_shop (player_id, last_refresh, `data`) VALUES ('$player_id', '" . time() . "', '" . json_encode($bullStock) . "')";
        $resultShop     = mysqli_query($con->connection(), $queryShop) or die(mysqli_error($con->connection()));


        // Inventory
        $queryInventory = "INSERT INTO lotfi_inventory (player_id, `data`) VALUES ('$player_id', '" . json_encode($bullInventory) . "')";
        $resultInventory= mysqli_query($con->connection(), $queryInventory) or die(mysqli_error($con->connection()));

        mysqli_close($con->connection());
    }
}

class BullInventory {
    // Class to process inventory
    public static function moveItem($from, $to, $player_id) {
        $con            = new BullSQL();

        $query          = "SELECT * FROM lotfi_inventory WHERE player_id='$player_id'";
        $result         = mysqli_query($con->connection(), $query) or die(mysqli_error($con->connection()));

        $data           = BullSQL::mysqli_result($result, 0, "data");
        $id             = BullSQL::mysqli_result($result, 0, "ID");


        // Decode
        $invent         = json_decode($data);

        // Cache it
        $tempObject     = $invent[$from];
        $tempObject2    = $invent[$to];

        // Swap now
        $invent[$to]    = $tempObject;
        $invent[$from]  = $tempObject2;

        // Swapped
        // Now update to db

        $query2         = "UPDATE lotfi_inventory SET `data`='" . json_encode($invent) . "' WHERE ID='$id'";
        $res2           = mysqli_query($con->connection(), $query2)
                        or die(
                            mysqli_error($con->connection())
                        );

        mysqli_close($con->connection());
    }

    public static function buy($from, $to, $player_id) {
        // The FROM var is the location of the item __in the shop__
        // The TO var is the location of the item __in the player inventory__
        // Get the shop from Player first
        $con            = new BullSQL();

        $query          = "SELECT * FROM lotfi_shop WHERE player_id='$player_id'";
        $shopResult     = mysqli_query($con->connection(), $query)
                            or die(
                                mysqli_error($con->connection())
                            );

        // Get inventory of the player
        $query          = "SLECT * FROM lotfi_inventory WHERE player_id='$player_id'";
        $invResult      = mysqli_query($con->connection(), $query)
                            or die(
                                mysqli_error($con->connection())
                            );

        $shopData       = BullSQL::mysqli_result($shopResult, 0, "data");
        $inventoryData  = BullSQL::mysqli_result($invResult,  0, "data");

        $shopInventory  = json_decode($shopData);
        $plrInventory   = json_decode($inventoryData);

        $tempObject1    = $shopInventory[$from];

        if($plrInventory[$to]["id"] == 0) {
            // There is empty slot, so check if we got any money
            $bull = new Bull($player_id);

            if(!$bull->gold >= $tempObject1["price"]) {
                // We don't have money
                BullDebug::addError("BullInventory: Buy $from $to at $player_id failed: No money");
                return false;
            }

            // Clear the shopInvetory space
            $shopInventory[$from]   = BullUtil::emptyItem();
            $plrInventory[$to]      = $tempObject1;

            $shopData               = json_encode($shopInventory);
            $inventoryData          = json_encode($plrInventory);

            $queryShop              = "UPDATE lotfi_shop SET `data`='$shopData' WHERE player_id = '$player_id'";
            $resultShop             = mysqli_query($con->connection(), $queryShop)
                                        or die(
                                            mysqli_error($con->connection())
                                        );

            $queryPlayer            = "UPDATE lotfi_inventory SET `data`='$inventoryData' WHERE player_id = '$player_id'";
            $resultPlayer           = mysqli_query($con->connection(), $queryPlayer)
                                        or die(
                                            mysqli_error($con->connection())
                                        );

            // Return true, we have bought it!
            BullDebug::addMsg("BullInventory: Bought $from $to");
            return true;
        }
        else {
            // Return false
            BullDebug::addError("BullInventory: Buy $from $to at $player_id failed: Inventory slot occupied");
            return false;
        }
    }

    public static function sell($from, $player_id) {
        $bull           = new Bull($player_id);
        $con            = new BullSQL();

        $query1         = "SELECT * FROM lotfi_inventory WHERE player_id='$player_id'";
        $result1        = mysqli_query($con->connection(), $query1) or die(mysqli_error($con->connection()));

        $inventoryRaw   = BullSQL::mysqli_result($result1, 0, "data");
        $inventoryData  = json_decode($inventoryRaw);

        $tempObject     = $inventoryData[$from];

        $bull->addGold($tempObject["price"] / 2); // Give gold to the player for selling the item

        $inventoryData[$from] = BullUtil::emptyItem(); // Set the slot to empty

        $inventoryRaw   = json_encode($inventoryData);

        $query2         = "UPDATE lotfi_inventory SET `data`='$inventoryRaw' WHERE player_id='$player_id'";
        $result2        = mysqli_query($con->connection(), $query2) or die(mysqli_error($con->connection()));

        BullDebug::addMsg("BulInventory: Sell $from at $player_id");

    }
}

class BullArena {
    public static function skillUP($skillName, $player_id) {
        $bull = new Bull($player_id);
        $con = BullSQL::returnNewConnection();

        if($skillName == "force") {
            if($bull->gold >= BullUtil::getPrice(10, $bull->force)) {
                $spent = $bull->gold - BullUtil::getPrice(10, $bull->force);
                $bull->force++;

                // Query
                $query = "UPDATE lotfi_stats
                    SET `force`='" . $bull->force . "', gold='$spent' WHERE player_id='$player_id'";

                $result = mysqli_query($con->connection(), $query)
                    or die(mysqli_error($con->connection()));

                Bull::addRandomExperience($con->connection(), $bull->force, $player_id);
                mysqli_close($con->connection());
            }
        }

        else if($skillName == "agility") {
            if($bull->gold >= BullUtil::getPrice(10, $bull->agility)) {
                $spent = $bull->gold - BullUtil::getPrice(10, $bull->agility);
                $bull->agility++;

                // Query
                $query = "UPDATE lotfi_stats
                    SET `agility`='" . $bull->agility . "', gold='$spent' WHERE player_id='$player_id'";

                $result = mysqli_query($con->connection(), $query)
                    or die(mysqli_error($con->connection()));

                Bull::addRandomExperience($con->connection(), $bull->agility, $player_id);
                mysqli_close($con->connection());

            }
        }

        else if($skillName == "endurance") {
            if($bull->gold >= BullUtil::getPrice(10, $bull->endurance)) {
                $spent = $bull->gold - BullUtil::getPrice(10, $bull->endurance);
                $bull->endurance++;

                // Query
                $query = "UPDATE lotfi_stats
                    SET `endurance`='" . $bull->endurance . "', gold='$spent' WHERE player_id='$player_id'";

                $result = mysqli_query($con->connection(), $query)
                    or die(mysqli_error($con->connection()));

                Bull::addRandomExperience($con->connection(), $bull->endurance, $player_id);
                mysqli_close($con->connection());
            }
        }

        else if($skillName == "speed") {
            if($bull->gold >= BullUtil::getPrice(10, $bull->speed)) {
                $spent = $bull->gold - BullUtil::getPrice(10, $bull->speed);
                $bull->speed++;

                // Query
                $query = "UPDATE lotfi_stats
                    SET `speed`='" . $bull->speed . "', gold='$spent' WHERE player_id='$player_id'";

                $result = mysqli_query($con->connection(), $query)
                    or die(mysqli_error($con->connection()));

                Bull::addRandomExperience($con->connection(), $bull->speed, $player_id);
                mysqli_close($con->connection());
            }
        }

        else if($skillName == "charge") {
            if($bull->gold >= BullUtil::getPrice(10, $bull->charge)) {
                $spent = $bull->gold - BullUtil::getPrice(10, $bull->charge);
                $bull->charge++;

                // Query
                $query = "UPDATE lotfi_stats
                    SET `charge`='" . $bull->charge . "', gold='$spent' WHERE player_id='$player_id'";

                $result = mysqli_query($con->connection(), $query)
                    or die(mysqli_error($con->connection()));

                Bull::addRandomExperience($con->connection(), $bull->charge, $player_id);
                mysqli_close($con->connection());
            }
        }
    }

    public static function getPrice($skillName, $player_id) {
        if($skillName == "force") {
            $bull = new Bull($player_id);
            return BullUtil::getPrice(10, $bull->force);
        }

        else if($skillName == "agility") {
            $bull = new Bull($player_id);
            return BullUtil::getPrice(10, $bull->agility);
        }

        else if($skillName == "endurance") {
            $bull = new Bull($player_id);
            return BullUtil::getPrice(10, $bull->endurance);
        }

        else if($skillName == "speed") {
            $bull = new Bull($player_id);
            return BullUtil::getPrice(10, $bull->speed);
        }

        else if($skillName == "charge") {
            $bull = new Bull($player_id);
            return BullUtil::getPrice(10, $bull->charge);
        }
    }
}

class BullShop {
    public static function refreshShop($player_id = "") {
        // refresh shop with the new items
        if (empty($player_id)) {
            // Server Based
            // Cron

        } else {
            // Player Request

            $con = new BullSQL();
            $items = BullUtil::getListOfItems($con);
            $newStock = BullUtil::generateNewItems($items, 5, 4);

            $query = "UPDATE lotfi_shop SET last_refresh='" . time() . "', `data` = '" . json_encode($newStock) . "' WHERE player_id='$player_id'";
            $result = mysqli_query($con->connection(), $query) or die(mysqli_error($con->connection()));

            mysqli_close($con->connection());
        }
    }



    // The following is made only for the example!
    public static function displayShopTab($x, $y, $player_id, $connection = "", $rawData = "") {
        if(empty($connection) and empty($rawData)) {
            // Establish a new connection!
            $connection = new BullSQL();
        }

        if(empty($rawData)) {
            $query = "SELECT * FROM lotfi_shop WHERE player_id='$player_id'";
            $result = mysqli_query($connection->connection(), $query) or die(mysqli_error($connection->connection()));

            $rawData = BullSQL::mysqli_result($result, 0, "data");
        }

        $data       = json_decode($rawData);

        $title  = "";
        $gems   = $data[$x][$y]->gems;


        for($i = 0; $i < sizeof($data[$x][$y]->gems); $i ++) {
            //var_dump($gems);
            /*$title .= $gems[$i]->name;
            $title .= " (" . $gems[$i]->bonus . ") for ";
            $title .= $gems[$i]->stat . ", ";*/
        }


        echo $data[$x][$y]->name . " - " . $data[$x][$y]->price . " <br>";
        echo "<span title='$title'>" . sizeof($data[$x][$y]->gems) . " gems</span>";
        echo "- <a href='example_actions.php?action=bfs&x=$x&y=$y'>BUY</a>";
    }

    public static function displayInventoryTab($x, $y, $player_id, $connection = "", $rawData = "") {
        if(empty($connection) and empty($rawData)) {
            // Establish a new connection!
            $connection = new BullSQL();
        }

        if(empty($rawData)) {
            $query = "SELECT * FROM lotfi_inventory WHERE player_id='$player_id'";
            $result = mysqli_query($connection->connection(), $query) or die(mysqli_error($connection->connection()));

            $rawData = BullSQL::mysqli_result($result, 0, "data");
        }

        $data       = json_decode($rawData);


        $title = "";
        echo $data[$x][$y]->name . " - " . $data[$x][$y]->price . " <br>";
        echo "<span title='$title'>" . sizeof($data[$x][$y]->gems) . " gems</span>";
        echo " - <a href='example_actions.php?action=sfi&x=$x&y=$y'>SELL</a>";

    }
}
