<?php
/**
 * Created by PhpStorm.
 * User: Aresak
 * Date: 7/20/2015
 * Time: 9:40 PM
 */

if(isset($_GET["action"])) {
    if($_GET["action"] == "new_player") {
        header("Location: example_actions.php?action=" . $_GET["action"] . "&callback=example.php");
    }
}

include "bullspj.php";

$player_id = $_GET["player_id"];
$bull = new Bull($player_id); // Our debug player id



?>
<h1>This is Example of our work</h1>
<b>Heading</b><br>
Golds: <?php echo $bull->gold; ?>
<form action="example_actions.php">
    <input type="hidden" name="callback" value="example.php">
    <input type="hidden" name="action" value="addgold">
    <input type="hidden" name="player_id" value="<?php echo $player_id; ?>">
    <input type="text" name="gold" placeholder="Golds">
</form>
Name: RandomPlayerName<br>
<a href="example_actions.php?action=new_player&callback=example.php&">Generate new player (Only in database, you are not able to switch it yet)</a>
<a href="example_actions.php?action=refresh_shop&callback=example.php&player_id=<?php echo $player_id; ?>">Refresh shop (CRON)</a>

<br><br>
<b>Arena</b>
<br>
Train Skillz: <br>
Force: <?php echo $bull->force; ?>&nbsp;&nbsp;&nbsp;
$<?php echo BullArena::getPrice("force", $player_id); ?>
<a href="example_actions.php?action=buy&skill=force&player_id=<?php echo $player_id; ?>&callback=example.php">
    Upgrade
</a>
<br>
Agility: <?php echo $bull->agility; ?>&nbsp;&nbsp;&nbsp;
$<?php echo BullArena::getPrice("agility", $player_id); ?>
<a href="example_actions.php?action=buy&skill=agility&player_id=<?php echo $player_id; ?>&callback=example.php">
    Upgrade
</a>
<br>
Endurance: <?php echo $bull->endurance; ?>&nbsp;&nbsp;&nbsp;
$<?php echo BullArena::getPrice("endurance", $player_id); ?>
<a href="example_actions.php?action=buy&skill=endurance&player_id=<?php echo $player_id; ?>&callback=example.php">
    Upgrade
</a>
<br>
Speed: <?php echo $bull->speed; ?>&nbsp;&nbsp;&nbsp;
$<?php echo BullArena::getPrice("speed", $player_id); ?>
<a href="example_actions.php?action=buy&skill=speed&player_id=<?php echo $player_id; ?>&callback=example.php">
    Upgrade
</a>
<br>
Charge: <?php echo $bull->charge; ?>&nbsp;&nbsp;&nbsp;
$<?php echo BullArena::getPrice("charge", $player_id); ?>
<a href="example_actions.php?action=buy&skill=charge&player_id=<?php echo $player_id; ?>&callback=example.php">
    Upgrade
</a>
<br>
Experience: <?php echo $bull->experience; ?><br>

<br>
Life: <?php echo BullUtil::getStatLife($bull->returnStats()); ?><br>
Critical Damage: <?php echo BullUtil::getStatCritical($bull->returnStats()); ?><br>
Damage Per Second: <?php echo BullUtil::getStatDamagePerSecond($bull->returnStats()); ?><br>


<br><br><br><br>
<b>Shop</b>

<?php

// Let fetch "few" the raw data from the user shop and inventory!
$connection = new BullSQL();

$queryShop          = "SELECT * FROM lotfi_shop WHERE player_id='$player_id'";
$resultShop         = mysqli_query($connection->connection(), $queryShop) or die(mysqli_error($connection->connection()));

$shopData           = BullSQL::mysqli_result($resultShop, 0, "data");


$queryInventory     = "SELECT * FROM lotfi_inventory WHERE player_id='$player_id'";
$resultInventory    = mysqli_query($connection->connection(), $queryInventory) or die(mysqli_error($connection->connection()));

$InventoryData      = BullSQL::mysqli_result($resultInventory, 0, "data");


?>

<b>This is Shop Window</b>
<table>
    <tr>
        <td>
            <?php
                echo BullShop::displayShopTab(0, 0, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(1, 0, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(2, 0, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(3, 0, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(4, 0, $bull->player_id, "", $shopData);
            ?>
        </td>
    </tr>


    <tr>
        <td>
            <?php
                echo BullShop::displayShopTab(0, 1, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(1, 1, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(2, 1, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(3, 1, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(4, 1, $bull->player_id, "", $shopData);
            ?>
        </td>
    </tr>


    <tr>
        <td>
            <?php
                echo BullShop::displayShopTab(0, 2, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(1, 2, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(2, 2, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(3, 2, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(4, 2, $bull->player_id, "", $shopData);
            ?>
        </td>
    </tr>


    <tr>
        <td>
            <?php
                echo BullShop::displayShopTab(0, 3, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(1, 3, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(2, 3, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(3, 3, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
                echo BullShop::displayShopTab(4, 3, $bull->player_id, "", $shopData);
            ?>
        </td>
    </tr>
</table>


<b>This is Inventory Window</b>
<table>
    <tr>
        <td>
            <?php
            echo BullShop::displayInventoryTab(0, 0, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(1, 0, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(2, 0, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(3, 0, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(4, 0, $bull->player_id, "", $shopData);
            ?>
        </td>
    </tr>


    <tr>
        <td>
            <?php
            echo BullShop::displayInventoryTab(0, 1, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(1, 1, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(2, 1, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(3, 1, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(4, 1, $bull->player_id, "", $shopData);
            ?>
        </td>
    </tr>


    <tr>
        <td>
            <?php
            echo BullShop::displayInventoryTab(0, 2, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(1, 2, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(2, 2, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(3, 2, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(4, 2, $bull->player_id, "", $shopData);
            ?>
        </td>
    </tr>


    <tr>
        <td>
            <?php
            echo BullShop::displayInventoryTab(0, 3, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(1, 3, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(2, 3, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(3, 3, $bull->player_id, "", $shopData);
            ?>
        </td>

        <td>
            <?php
            echo BullShop::displayInventoryTab(4, 3, $bull->player_id, "", $shopData);
            ?>
        </td>
    </tr>
</table>