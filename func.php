<?php
/**
 * Created by PhpStorm.
 * User: Aresak
 * Date: 7/12/2015
 * Time: 12:44 PM
 */


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function getBaseSkill() {
    return 10;
}

function getRefreshPrice() {
    return 50;
}

function getBaseItem() {
    return 10;
}

function getMaxEXP() {
    return 50000;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function getPrice($base, $level) {
    $res = $base;
    for($i = 0; $i < $level; $i++) {
        $res += $base + $res;
    }

    return $res;
}


function getItemId($name, $connection = "") {
    if($connection == "") {
        if(!isset(SQLInfo::$user)) return;
        else {
            $connection = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
            or die(mysqli_error($connection));
        }
    }

    $query = "SELECT * FROM lotfi_items";
    $result = mysqli_query($connection, $query)
    or die(mysqli_error($connection));


    for($a = 0; $a < mysqli_num_rows($result); $a ++) {
        if(mysqli_result($result, $a, "name") == $name) {
            return mysqli_result($result, $a, "ID");
        }
    }
}

function getItemName($id, $connection = "") {
    if($connection == "") {
        if(!isset(SQLInfo::$user)) return;
        else {
            $connection = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
            or die(mysqli_error($connection));
        }
    }

    $query = "SELECT * FROM lotfi_items";
    $result = mysqli_query($connection, $query)
    or die(mysqli_error($connection));


    for($a = 0; $a < mysqli_num_rows($result); $a ++) {
        if(mysqli_result($result, $a, "ID") == $id) {
            return mysqli_result($result, $a, "name");
        }
    }
}

function generateNewItems($items, $slots) {
    $result = array();
    for($i = 0; $i < $slots; $i ++) {
        $randomness = rand(0, sizeof($items));

        $result[$i]["id"] = $items[$randomness]["id"];
        $result[$i]["name"] = $items[$randomness]["name"];
    }

    return $result;
}

function addItemToInventory($inventory, $item) {
    $result = array();

    for($i = 0; $i < sizeof($inventory); $i ++) {
        $result[$i]["name"] = $inventory[$i]["name"];
        $result[$i]["id"]   = $inventory[$i]["id"];
        $result[$i]["level"]= $inventory[$i]["level"];
    }

    $result[(sizeof($inventory) + 1)]["name"] = $item["name"];
    $result[(sizeof($inventory) + 1)]["id"] = $item["id"];
    $result[(sizeof($inventory) + 1)]["level"] = $item["level"];

    return $result;
}

function renderTableInvetory($id, $action) {
    if(!isset(SQLInfo::$user)) return;

    $connection = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
        or die(mysqli_error($connection));

    $query = "SELECT * FROM lotfi_shop WHERE player_id='$id'";
    $result = mysqli_query($connection, $query)
        or die(mysqli_error($connection));


    $data  = mysqli_result($result, 0, "data");
    $eData = json_decode($data);


    for($y = 0; $y < 4; $y ++) {
        echo "<tr>";
        for($x = 0; $x < 5; $x ++) {
            if($eData[$x][$y]["item-id"] >! 0) $eData[$x][$y]["item-id"] = "empty";

            echo "<td class='inventory-item tablecoords$x-$y item-";
            echo $eData[$x][$y]["item-id"];
            echo "' onClick='$action($x, $y, ";
            echo $eData[$x][$y]["item-id"];
            echo ")'>";
            echo $eData[$x][$y]["item"]["name"];
            echo "</td>";
        }
        echo "</tr>";
    }
}


function getStatLife($stats) {
    return ($stats["force"] * 2.1 + $stats["endurance"] * 3) / 1.5 + $stats["experience"] * 0.01;
}

function getStatDamagePerSecond($stats) {
    return ($stats["speed"] * 0.4 + $stats["force"] * 1.4) * 0.77 + 0.02;
}

function getStatCrital($stats) {
    return pow($stats["force"] * $stats["speed"], 2) / 1000 + $stats["agility"] * 6 + $stats["experience"] * 0.01;
}

function getStats($force, $agility, $endurance, $speed, $charge, $experience, $gold = "") {
    $result["force"] = $force;
    $result["agility"] = $agility;
    $result["endurance"] = $endurance;
    $result["speed"] = $speed;
    $result["charge"] = $charge;
    $result["experience"] = $experience;
    $result["gold"] = $gold;

    return $result;
}

function addRandomExperience($connection, $base, $where_id) {
    $min = 1 * $base;
    $max = $base * $base;

    $total = rand($min, $max);

    $query = "UPDATE lotfi_stats SET experience = experience + $total WHERE player_id='$where_id'";
    $result = mysqli_query($connection, $query)
        or die(mysqli_error($connection));

    return $total;
}
