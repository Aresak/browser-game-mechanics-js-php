/**
 * Created by Aresak on 7/12/2015.
 */
function getPrice(base, level) {

    var res = base;
    for(var i = 0; i < level; i ++) {
        res += base + res;
    }

    return res;
}

function getLife() {
    return (lotfi.force * 2.1 + lotfi.endurance * 3) / 1.5 + lotfi.experience * 0.01;
}

function getDamagePerSecond() {
    return (lotfi.speed * 0.4 + lotfi.force * 1.4) * 0.77 + lotfi.experience * 0.02;
}

function getCriticalDamage() {
    return Math.sqrt(lotfi.force * lotfi.speed) / 1000 + lotfi.agility * 6 + lotfi.experience * 0.01;
}

function shopItemClicked(x, y, id) {

}

function inventoryItemClicked(x, y, id) {

}