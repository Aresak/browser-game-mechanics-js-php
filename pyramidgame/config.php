<?php
/**
 * Created by PhpStorm.
 * User: SYSTEM
 * Date: 30.8.2015
 * Time: 10:46
 */

date_default_timezone_set("UTC");
define("ARE_DATABASE_FILE_PATH", "../database.php");

define("ARE_DATABASE_TABLE_PREFIX", "pg_");


// Game settings
define("GAME_TIMER_NEWGAME", 120);
define("GAME_TIMER_Entries", 60);
define("GAME_TIMER_LockUp", 5);
define("GAME_TIMER_Calcul", 5);
define("GAME_TIMER_Winners", 10);


// SQL security stuff
define("allowed_to_view_database_info", "true");
define("sql_general_salt", "iNwlrcdAi00omnoWieseWse1s7ZZZ");