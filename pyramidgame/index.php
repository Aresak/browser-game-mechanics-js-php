<?php
/**
 * Created by PhpStorm.
 * User: SYSTEM
 * Date: 30.8.2015
 * Time: 11:19
 */
include "config.php";
include ARE_DATABASE_FILE_PATH;

    if(isset($_POST)) {
        if(isset($_POST["playerid"])) {
            $loggedin = true;
            $playerid = $_POST["playerid"];
        }
    }
    else {
        $loggedin = false;
        $playerid = -null;
    }
?>

<html>
    <head>
        <title>The Pyramid Game</title>
        <meta charset="UTF-8">
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <style>
            body {
                font-family: Arial;
            }

            .slotgraph {
                width: 32px;
                height: 32px;
            }

            .locked_slot {
                cursor: not-allowed;
            }

            .opened_slot {
                cursor: pointer;
            }

            .closed-room {
                width: 32px;
                height: 32px;
            }

            .selected-slot {
                cursor: progress;
            }

            .status-1 {
                font-weight: bold;
            }

            .status-2 .status-3 .status-4 {
                color: red;
            }
        </style>
    </head>
    <body>
        <?php

            if(!$loggedin) {
                // show up the login page

                ?>
                    <b>
                        Please enter your name to log in.<br>
                        <i>
                            To create a new account just simply put any name you want, if it doesn't exists,<br>
                            we will create you one
                        </i>
                    </b>
                    <br><br><br>
                    <form action="login.php" method="post">
                        Name: <input type="text" name="name" placeholder="Your unique username">
                        <br>
                        <button>Get into the game now!</button>
                    </form>
                <?php
            }
            else {
                // Already logged in
                // Get player data

                $connection         = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
                                or die(mysqli_error($connection));

                $playerDataQuery    = "SELECT * FROM " . ARE_DATABASE_TABLE_PREFIX . "players WHERE ID='$playerid'";
                $playerDataResult   = mysqli_query($connection, $playerDataQuery);

                $playerWins         = mysqli_result($playerDataResult, 0, "wins");
                $playerName         = mysqli_result($playerDataResult, 0, "name");
                $playerLooses       = mysqli_result($playerDataResult, 0, "looses");
                $playerDataRowWin   = mysqli_result($playerDataResult, 0, "rowwin");
                $playerCoins        = mysqli_result($playerDataResult, 0, "coins");

                echo "Debug Player ID <span class='player_id'>" . $playerid . "</span>, name: <span class='player_name'>" . $playerName . "</span>, coins: <span class='player_coins'>" . $playerCoins . "</span>, wins/loses: <span class='player_wins'>" . $playerWins . "</span>/<span class='player_loses'>" . $playerLooses . "</span>, in a row of: <span class='player_rowWin'>" . $playerDataRowWin . "</span> wins";

                ?>
                    <script>
                        var info = {
                            update: 1
                        };

                        // Player Meta Info
                        var player = {
                            name: "<?php echo $playerName; ?>",
                            id: "<?php echo $playerid; ?>",
                            coins: "<?php echo $playerCoins; ?>",
                            wins: "<?php echo $playerWins; ?>",
                            loses: "<?php echo $playerLooses; ?>",
                            rowWin: "<?php echo $playerDataRowWin; ?>",
                            refreshStats: function() {
                                console.log("(" + info.update + ") Updating Player Stats...");
                                $.post("ajax.php", {request: "update_stats", player: player.id}, function(data) {
                                    console.log(data);
                                    var dataObject  = JSON.parse(data);
                                    player.name     = dataObject.name;
                                    player.id       = dataObject.id;
                                    player.coins    = dataObject.coins;
                                    player.wins     = dataObject.wins;
                                    player.loses    = dataObject.loses;
                                    player.rowWin   = dataObject.rowWin;

                                    $(".player_name").each(function(index) {$(this).text(player.name);});
                                    $(".player_id").each(function(index) {$(this).text(player.id);});
                                    $(".player_coins").each(function(index) {$(this).text(player.coins);});
                                    $(".player_wins").each(function(index) {$(this).text(player.wins);});
                                    $(".player_loses").each(function(index) {$(this).text(player.loses);});
                                    $(".player_rowWin").each(function(index) {$(this).text(player.rowWin);});
                                    info.update++;
                                    setTimeout(player.refreshStats, 10000);
                                }, "html");
                            }
                        };

                        var lobby = {
                            refresh: function() {
                                console.log("Refreshing games' list");
                                $.post("ajax.php", {request: "get_games_list", player: player.id}, function(data) {
                                    $("#list_of_games").html(data);
                                }, "html");
                            },
                            newRoom: function() {
                                console.log("Creating new game room");
                                $.post("ajax.php", {request: "new_game_room", player: player.id}, function(data) {
                                    if(data.split(" ")[0] != "success") {
                                        console.error(data);
                                    }
                                    else {
                                        console.log("Room created ID " + data.split(" ")[1]);
                                        lobby.join(data.split(" ")[1]);
                                    }
                                }, "html");
                            },
                            status: function(id) {
                                var sts;
                                if      (id == 1) sts = "start the game";
                                else if (id == 2) sts = "waiting for entries";
                                else if (id == 3) sts = "calculate the results";
                                else if (id == 4) sts = "shout out the winners";
                                else if (id == 5) sts = "start the new game. Leveling up!";
                                else sts = "unknown";
                                return sts;
                            },
                            join: function(id) {
                                console.log("Connecting to lobby ID " + id);
                                $.post("ajax.php", {request: "join_room", player: player.id, room: id}, function(data) {

                                    var object = JSON.parse(data);
                                    /** @namespace object.success */
                                    /** @namespace object.level */
                                    /** @namespace object.starttime */
                                    /** @namespace object.status */
                                    /** @namespace object.entrycosts */
                                    /** @namespace object.creator */
                                    /** @namespace object.errormessage */
                                    /** @namespace object.synctime */

                                    if(object.success) {
                                        game.level = object.level;
                                        game.id = object.id;
                                        game.starttime = object.starttime;
                                        game.status = object.status;
                                        game.entrycosts = object.entrycosts;
                                        game.creator = object.creator;
                                        game.synctime = object.synctime;
                                        game.connected = true;


                                        $("#games_list").hide();
                                        game.setupGraphics();


                                        game.timer(object.synctime, lobby.status(game.status));

                                        $("#game_window").show();
                                    }
                                    else if(object.errormessage.length > 0) {
                                        console.error(object.errormessage);
                                    }

                                }, "html");
                            },
                            synchronizeTime: function() {
                                var date = new Date();
                                $("#game-time").html(date.getUTCDate() + "." + (date.getUTCMonth() + 1) + "." + date.getUTCFullYear() + " "
                                + date.getUTCHours() + ":" + date.getUTCMinutes() + ":" + date.getUTCSeconds() + ":" + date.getUTCMilliseconds());
                                setTimeout(lobby.synchronizeTime, 9);
                            }
                        };

                        var game = {
                            level: 0,
                            id: 0,
                            starttime: 0,
                            synctime: 0,
                            status: 0,
                            entrycosts: 0,
                            creator: 0,
                            currentTime: 0,
                            connected: true,
                            voteFree: false,
                            triangle: function(level, slot) {
                                if(!game.voteFree) return;
                                $.post("ajax.php", {player: player.id, room: game.id, slot: slot, level: level, request: "vote"}, function(data) {
                                    console.log(data);
                                    var object = JSON.parse(data);
                                    if(object.success) {
                                        console.log("Voted for " + slot);
                                        game.synctime = object.synctime;
                                        game.voteFree = false;
                                        game.lockall();
                                        game.voted(level, slot);
                                    }
                                    else {
                                        console.error(object.errormessage);
                                        game.disconnect();
                                    }
                                }, "html")
                            },
                            unlock: function(level) {
                                for(var slot = 1; slot <= (level + 1); slot ++) {
                                    $("#t" + level + "-" + slot).attr("class", "opened_slot");
                                }

                                game.setupGraphics();
                            },
                            lock: function(level) {
                                for(var slot = 1; slot <= (level + 1); slot ++) {
                                    $("#t" + level + "-" + slot).attr("class", "locked_slot");
                                }
                                game.setupGraphics();
                            },
                            lockall: function() {
                                for(var i = 1; i <= 7; i ++) {
                                    game.lock(i);
                                }
                            },
                            unlockLevel: function(level) {
                                for(var i = 1; i <= 7; i ++) {
                                    if(i != level) {
                                        game.lock(i);
                                    }
                                    else {
                                        game.unlock(i);
                                    }
                                }
                            },
                            voted: function(level, slot) {
                                $("#t" + level + "-" + slot).attr("class", "selected_slot");
                                game.setupGraphics();
                            },
                            setupGraphics: function() {
                                $(".locked_slot").each(function() {$(this).html("<image class='slotgraph' src='https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/128/lock.png' title='This slot is locked'>")});
                                $(".opened_slot").each(function() {$(this).html("<image class='slotgraph' src='https://cdn1.iconfinder.com/data/icons/user-actions/48/lock_user_employee_account_access_seize_funds-128.png' title='Click to seize slot'>")});
                                $(".selected_slot").each(function() {$(this).html("<image class='slotgraph' src='http://3.bp.blogspot.com/_VS9cwv9dkqY/SQMzSsMfTdI/AAAAAAAAAi0/fGOuEFz4PY4/s400/i+voted.gif' title='Selected slot'>");});
                            },
                            timer: function(duration, reason) {

                                var diff, seconds, startTime = Date.now();

                                function tm() {
                                    diff = duration - (((Date.now() - startTime) / 1000) | 0);

                                    seconds = (diff % 10000) | 0;
                                    seconds = seconds < 10 ? "0" + seconds : seconds;

                                    $("#timer0").text(seconds);
                                    $("#timer1").text(reason);

                                    if(game.connected) {
                                        if(diff <= 0) {
                                            game.timerPrefab(reason);
                                        }
                                        else {
                                            setTimeout(tm, 1000);
                                        }
                                    }
                                };
                                tm();

                            },
                            timerPrefab: function(reason) {
                                if(reason == "start the game") {
                                    $.post("ajax.php", {request: "time_syncs1", player: player.id, room: game.id}, function(data) {
                                        console.log(data);
                                        var object = JSON.parse(data);

                                        if(object.success) {
                                            game.unlockLevel(8 - object.level);
                                            game.level = object.level;
                                            game.synctime = object.synctime;
                                            game.voteFree = true;

                                            // Sync time now
                                            game.timer(object.synctime, lobby.status(object.status));
                                        }
                                        else {
                                            console.error(object.errormessage);
                                            game.disconnect();
                                        }
                                    }, "html");
                                }
                                else if(reason == "waiting for entries") {
                                    game.lockall();
                                    $.post("ajax.php", {request: "time_syncs2", player: player.id, room: game.id}, function(data) {
                                        console.log(data);
                                        var object = JSON.parse(data);

                                        if(object.success) {
                                            game.level = object.level;
                                            game.synctime = object.synctime;

                                            // Sync time now
                                            game.timer(object.synctime, lobby.status(object.status));
                                        }
                                        else {
                                            console.error(object.errormessage);
                                            game.disconnect();
                                        }
                                    }, "html");
                                }
                                else if(reason == "calculate the results") {
                                    $.post("ajax.php", {request: "time_syncs3", player: player.id, room: game.id}, function(data) {
                                        console.log(data);
                                        var object = JSON.parse(data);

                                        if(object.success) {
                                            game.level = object.level;
                                            game.synctime = object.synctime;

                                            // Sync time now
                                            game.timer(object.synctime, lobby.status(object.status));
                                        }
                                        else {
                                            console.error(object.errormessage);
                                            game.disconnect();
                                        }
                                    }, "html");
                                }
                                else if(reason == "shout out the winners") {
                                    $.post("ajax.php", {request: "time_syncs4", player: player.id, room: game.id}, function(data) {
                                        console.log(data);
                                        var object = JSON.parse(data);

                                        if(object.success) {
                                            game.unlockLevel(8 - object.level);
                                            game.level = object.level;
                                            game.synctime = object.synctime;

                                            // leveld up:
                                            game.accepted   = object.accepted;
                                            if(game.accepted) {
                                                game.receive = object.receive;
                                            }

                                            // Sync time now
                                            game.timer(object.synctime, lobby.status(object.status));
                                        }
                                        else {
                                            console.error(object.errormessage);
                                            game.disconnect();
                                        }
                                    }, "html");
                                }
                                else if(reason == "start the new game. Leveling up!") {

                                }
                            },
                            disconnect: function() {
                                $("#games_list").show();
                                $("#game_window").hide();
                                game.connected = false;
                            }
                        };

                        setTimeout(player.refreshStats, 10000);
                        setTimeout(lobby.synchronizeTime, 9);
                        setTimeout(lobby.refresh, 1000);

                    </script>
                    <div id="game_content">
                        <div id="games_list">
                            <a href="#" onClick="lobby.refresh();">Refresh list</a> | <a href="#" onClick="lobby.newRoom();">New Room</a><br>
                            <div id="list_of_games">Loading list...</div>
                        </div>
                        <div id="game_window" style="display: none;">
                            <a href="#" onClick="game.disconnect();">Disconnect</a>
                            <br><br>
                            <div id="triangle" style="margin-left: 25%;">
                                <span style="margin-left: 120px;"></span>
                                <span onClick="game.triangle(1,1)" id="t1-1" class="locked_slot"></span>
                                <span onClick="game.triangle(1,2)" id="t1-2" class="locked_slot"></span>
                                <br>
                                <span style="margin-left: 100px;"></span>
                                <span onClick="game.triangle(2,1)" id="t2-1" class="locked_slot"></span>
                                <span onClick="game.triangle(2,2)" id="t2-2" class="locked_slot"></span>
                                <span onClick="game.triangle(2,3)" id="t2-3" class="locked_slot"></span>
                                <br>
                                <span style="margin-left: 80px;"></span>
                                <span onClick="game.triangle(3,1)" id="t3-1" class="locked_slot"></span>
                                <span onClick="game.triangle(3,2)" id="t3-2" class="locked_slot"></span>
                                <span onClick="game.triangle(3,3)" id="t3-3" class="locked_slot"></span>
                                <span onClick="game.triangle(3,4)" id="t3-4" class="locked_slot"></span>
                                <br>
                                <span style="margin-left: 60px;"></span>
                                <span onClick="game.triangle(4,1)" id="t4-1" class="locked_slot"></span>
                                <span onClick="game.triangle(4,2)" id="t4-2" class="locked_slot"></span>
                                <span onClick="game.triangle(4,3)" id="t4-3" class="locked_slot"></span>
                                <span onClick="game.triangle(4,4)" id="t4-4" class="locked_slot"></span>
                                <span onClick="game.triangle(4,5)" id="t4-5" class="locked_slot"></span>
                                <br>
                                <span style="margin-left: 40px;"></span>
                                <span onClick="game.triangle(5,1)" id="t5-1" class="locked_slot"></span>
                                <span onClick="game.triangle(5,2)" id="t5-2" class="locked_slot"></span>
                                <span onClick="game.triangle(5,3)" id="t5-3" class="locked_slot"></span>
                                <span onClick="game.triangle(5,4)" id="t5-4" class="locked_slot"></span>
                                <span onClick="game.triangle(5,5)" id="t5-5" class="locked_slot"></span>
                                <span onClick="game.triangle(5,6)" id="t5-6" class="locked_slot"></span>
                                <br>
                                <span style="margin-left: 20px;"></span>
                                <span onClick="game.triangle(6,1)" id="t6-1" class="locked_slot"></span>
                                <span onClick="game.triangle(6,2)" id="t6-2" class="locked_slot"></span>
                                <span onClick="game.triangle(6,3)" id="t6-3" class="locked_slot"></span>
                                <span onClick="game.triangle(6,4)" id="t6-4" class="locked_slot"></span>
                                <span onClick="game.triangle(6,5)" id="t6-5" class="locked_slot"></span>
                                <span onClick="game.triangle(6,6)" id="t6-6" class="locked_slot"></span>
                                <span onClick="game.triangle(6,7)" id="t6-7" class="locked_slot"></span>
                                <br>
                                <span onClick="game.triangle(7,1)" id="t7-1" class="locked_slot"></span>
                                <span onClick="game.triangle(7,2)" id="t7-2" class="locked_slot"></span>
                                <span onClick="game.triangle(7,3)" id="t7-3" class="locked_slot"></span>
                                <span onClick="game.triangle(7,4)" id="t7-4" class="locked_slot"></span>
                                <span onClick="game.triangle(7,5)" id="t7-5" class="locked_slot"></span>
                                <span onClick="game.triangle(7,6)" id="t7-6" class="locked_slot"></span>
                                <span onClick="game.triangle(7,7)" id="t7-7" class="locked_slot"></span>
                                <span onClick="game.triangle(7,8)" id="t7-8" class="locked_slot"></span>
                            </div>

                            <h1>Timer: <span id="timer0"></span>s to <span id="timer1"></span></h1>

                        </div>
                    </div>

                <?php
            }

        ?>
        <br><br><br><br><br>
        Become a god and change anything you want: <i>(Stats are updating every 10s)</i><br>
        <script>
            function update(item) {
                var value = $("#god-" + item).val();
                $.post("ajax.php", {
                    request: "god_change",
                    player: player.id,

                    item: item,
                    value: value
                }, function(data) {
                    console.log(data);
                }, "html");
            }
        </script>
        ID: <input type="text" id="god-id"> <button onClick="update('id')">Update!</button><br>
        Name: <input type="text" id="god-name"> <button onClick="update('name')">Update!</button><br>
        Coins: <input type="text" id="god-coins"> <button onClick="update('coins')">Update!</button><br>
        Wins: <input type="text" id="god-wins"> <button onClick="update('wins')">Update!</button><br>
        Loses: <input type="text" id="god-looses"> <button onClick="update('looses')">Update!</button><br>
        Wins in a row: <input type="text" id="god-rowwin"> <button onClick="update('rowwin')">Update!</button><br>

        <br><br>
        Game Time: <span id="game-time">0.0.0000 00:00:00:000</span>
    </body>
</html>