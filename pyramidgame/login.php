<?php
/**
 * Created by PhpStorm.
 * User: SYSTEM
 * Date: 30.8.2015
 * Time: 11:40
 */

if(!isset($_POST)) {
    die();
}

include "config.php";
include ARE_DATABASE_FILE_PATH;

$connection         = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
                or die(mysqli_error($connection));

$checkQuery = "SELECT * FROM " . ARE_DATABASE_TABLE_PREFIX . "players WHERE `name`='" . mysqli_escape_string($connection, $_POST["name"]) . "'";
$checkResult = mysqli_query($connection, $checkQuery) or die(mysqli_error($connection));

if(mysqli_num_rows($checkResult) > 0) {
    // The user with this name already exists, return ID

    ?>
    Please Wait...
    <form action='index.php' method='post' name='frm'>
        <?php
            echo "<input type='hidden' name='playerid' value='" . mysqli_result($checkResult, 0, 'ID') . "'>";
        ?>
    </form>
    <script language="JavaScript">
        document.frm.submit();
    </script>

    <?php
}
else {
    // Player doesn't exists, create the name

    $createQuery = "INSERT INTO " . ARE_DATABASE_TABLE_PREFIX . "players (name) VALUES ('" . mysqli_escape_string($connection, $_POST["name"]) . "')";
    $createResult = mysqli_query($connection, $createQuery) or die(mysqli_error($connection));

    // Get the name now

    $check2Query = "SELECT * FROM " . ARE_DATABASE_TABLE_PREFIX . "players WHERE `name`='" . mysqli_escape_string($connection, $_POST["name"]) . "'";
    $check2Result = mysqli_query($connection, $checkQuery) or die(mysqli_error($connection));

    ?>
    Please Wait...
    <form action='index.php' method='post' name='frm'>
        <?php
        echo "<input type='hidden' name='playerid' value='" . mysqli_result($check2Result, 0, 'ID') . "'>";
        ?>
    </form>
    <script language="JavaScript">
        document.frm.submit();
    </script>

<?php
}