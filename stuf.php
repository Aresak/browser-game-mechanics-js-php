<?php
/**
 * Created by PhpStorm.
 * User: Aresak
 * Date: 7/12/2015
 * Time: 11:59 AM
 */

// fetchin data
define("allowed_to_view_database_info", true);
define("logged_in", true);
include "database.php";
include "func.php";

// Debug player id
$player_id = 1;

$connection = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
or die(mysqli_error($connection));

$query = "SELECT * FROM lotfi_stats WHERE player_id='$player_id'";
$result = mysqli_query($connection, $query)
or die(mysqli_error($connection));

$force          = mysqli_result($result, 0, "force");
$agility        = mysqli_result($result, 0, "agility");
$endurance      = mysqli_result($result, 0, "endurance");
$speed          = mysqli_result($result, 0, "speed");
$charge         = mysqli_result($result, 0, "charge");
$gold           = mysqli_result($result, 0, "gold");
$experience     = mysqli_result($result, 0, "experience");


?>
<html>
<head>
    <title>Lotfi Arena</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="lotfi.css">
    <script>
        var lotfi = {
            var basePriceSkill: 0<?php echo getBaseSkill(); ?>,
            var force: 0<?php echo $force;?>,
            var agility: 0<?php echo $agility;?>,
            var endurance: 0<?php echo $endurance;?>,
            var speed: 0<?php echo $speed;?>,
            var charge: 0<?php echo $charge;?>,
            var experience: 0<?php echo $experience;?>
        };
    </script>
    <script src="lotfi.js"></script>
</head>

<body>
    <button id="back-button" onClick="location.href = '/';">
        Back to index
    </button>


    <div id="menu">
        <?php
        // Php based menu
        // Debug form
        echo "<form method='POST' action='make_it_easy.php'>";
        echo "<input type='text' name='golds' placeholder='add golds'>";
        echo "<input type='hidden' name='callback' value='stuf.php'>";
        echo "</form>";

        // Debug time
        echo time();

        echo " | ";

        // golds
        echo "Gold: <span  id='player_gold'>" . $gold . "</span>";

        echo " | ";

        // Name last
        echo "PlayerName";
        ?>
    </div>

    <div id="player">
        <div id="shop">
            <div id="offers">
                <table>
                    <?php
                    renderTableInvetory($player_id, "shopItemClicked");
                    ?>
                </table>
            </div>

            <div id="inventory">
                <table>
                    <?php
                    renderTableInvetory($player_id, "inventoryItemClicked");
                    ?>
                </table>
            </div>
        </div>
    </div>
</body>
</html>